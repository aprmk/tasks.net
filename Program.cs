﻿using System;

namespace Test
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Magazine m = new Magazine();
            m.ToShortString();
            Console.WriteLine(m.ToShortString());
            Console.WriteLine(m[Frequency.Weekly]);
            Console.WriteLine(m[Frequency.Montly]);
            Console.WriteLine(m[Frequency.Yearly]);
            
            
            m.NameArticle = "The Garage";
            m.DateRelease = DateTime.Now;
            m.Periodicity = Frequency.Montly;
            m.Circulation = 3;
            Console.WriteLine(m.ToShortString());
            Article a= new Article();
            Article a2 = new Article();
            m.AddArticles(a, a2, null);  
            
            Console.WriteLine(m.ToString());
            
            string str = Console.ReadLine();
            string[] words = new string[2];
            if (str != null) words = str.Split(' ', ',');
            var nRows = Convert.ToInt32(words[0]);
            var nColumns = Convert.ToInt32(words[1]);
            int nCount = nRows * nColumns;
            Console.WriteLine(string.Format("nRows: {0}  nColumns: {1}", nRows, nColumns));
            
            
            Magazine[] oneDimArray = new Magazine[nCount];
            for (int i = 0; i < nCount; i++)
            {
                oneDimArray[i] = new Magazine();
                oneDimArray[i].AddArticles(a, a, a, a, a);
            }
            
            

            Magazine[,] twoDimArray = new Magazine[nRows, nColumns];
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    twoDimArray[i, j] = new Magazine();
                    twoDimArray[i,j].AddArticles(a, a, a, a, a);
                }
            } 
            
            //TODO Ступінчастий масив 
            Magazine[][] steppedArray = new Magazine[nRows][];
            for (int i = 0; i < nRows; i++)
            {
                steppedArray[i] = new Magazine[nColumns];
            }
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    steppedArray[i][j] = new Magazine();
                    steppedArray[i][j].AddArticles(a, a, a, a, a);
                }
            }
            
            int sum = 0, countt = 0;
            while (sum <= nCount)
            {
                sum += countt++;
            }

            Magazine[][] steppedArrayDifferent = new Magazine[countt--][];
            for (int i = 0, count = 0; count < countt; i++)
            {
                steppedArrayDifferent[i] = new Magazine[count + 1];
                for (int j = 0; j < count; j++)
                {
                    steppedArrayDifferent[i][j] = new Magazine();
                    steppedArrayDifferent[i][j].AddArticles(a, a, a, a, a);
        
                }
                count++;
            }

            //TODO Тестування по часу
            double tmp = 0;
            Console.WriteLine("OneArrayTest");
            var start = Environment.TickCount;
            for (int i = 0; i < nCount; i++)
            {
                tmp = oneDimArray[i].Seredn;
            }
            var finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            Console.WriteLine("TwoArrayTest");
            start = Environment.TickCount;
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    tmp = twoDimArray[i,j].Seredn;
                }
    
            }
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            Console.WriteLine("SteppedArrayTest");
            start = Environment.TickCount;
            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    tmp = steppedArray[i][j].Seredn;
                }
    
            }
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);

            Console.WriteLine("SteppedArrayDifferentTest");
            start = Environment.TickCount;
            for (int i = 0, count = 0; count < countt; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    tmp = steppedArrayDifferent[i][j].Seredn;
        
                }
                count++;
            }
            finish = Environment.TickCount;
            Console.WriteLine(finish - start);
            
            
            
        }
    }
}