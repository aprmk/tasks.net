﻿using System.Security.Principal;
using System.Text;

namespace Test
{
    public class Article
    {
       public Person AuthorArticle{ get; set; }
       public string NameArticle{ get; set; }
       public double RatingArticle{ get; set; }

       public Article(Person authorArticle, string nameArticle, double ratingArticle)
       {
           AuthorArticle = authorArticle;
           NameArticle = nameArticle;
           RatingArticle = ratingArticle;
    }

       public Article() : this(new Person(), "<SAVE>", 4)
       {
       }
       
       public override string ToString()
       {
           var sb = new StringBuilder();
           
           sb.Append("authorArticle");
           sb.Append(AuthorArticle);
           sb.Append( "\n");
           
           sb.Append("nameArticle: ");
           sb.Append(NameArticle);
           sb.Append( "\n");
           
           sb.Append("ratingArticle: ");
           sb.Append(RatingArticle);
           sb.Append( "\n");
          
           return sb.ToString();
       }
    }
}